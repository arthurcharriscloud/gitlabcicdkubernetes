FROM       node:latest
EXPOSE     3000
WORKDIR    /opt/tic-tac-toe
ADD        ./tic-tac-toe /opt/tic-tac-toe
RUN        npm install
CMD        npm start
